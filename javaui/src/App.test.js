import React from 'react';
import { shallow } from 'enzyme';
import App from './App';
describe('First React component test with Enzyme', () => {
  it('renders without crashing', () => {
    shallow(<App />);
  });
  it('should show correct text', () => {
    const wrapper = shallow(<navbar-brand />);
    expect(wrapper.text().includes('About')).toBe(true);
  });
});