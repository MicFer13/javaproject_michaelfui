import {applyMiddleware, createStore} from "redux";
import  PaymentListReducer from "./reducers/PaymentListReducer"
import thunk from "redux-thunk";
const initialState = {};
export const middlewares = [thunk];
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ //|| compose;

 const store = createStore(
    PaymentListReducer, //typically combine all reducers
    initialState,
    //  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    composeEnhancer(applyMiddleware(...middlewares))
);
//,
//composeEnhancer(applyMiddleware(...middlewares))
export default store;