import {PAYMENT_LIST_REQUEST , PAYMENT_LIST_SUCCESS , PAYMENT_LIST_FAIL}
    from "../constants/PaymentConstants"


import PaymentService from "../../services/PaymentRestService";
const employeeList = () =>  (dispatch) => {
    try {
        console.log(">>> in dispatch ");
        dispatch({ type: PAYMENT_LIST_REQUEST });
        const data =  PaymentService.getAll().then(response => {
            dispatch({ type: PAYMENT_LIST_SUCCESS, payload: response.data });
        });

    } catch (error) {
        console.log(">>> in dispatch error=" + error);
        dispatch({ type: PAYMENT_LIST_FAIL, payload: error });
    }
};
export  default employeeList;
