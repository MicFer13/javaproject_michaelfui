import http from "../http-common"
export class PaymentRestService{
    getAll(){
        return http.get("/all")
    }

    get(id){
        return http.get(`/findById/${id}`)

    }
    get(type){
        return http.get(`/findByType/${type}`)

    }
    create(data){
        return http.post("/save", data)

    }
}

export default  new PaymentRestService()