import React from 'react';
import logo from './logo.jpg';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { Switch, Route, Link } from "react-router-dom";
import Payment from "./components/ViewPayments"
import ListPayments from "./components/ListPayments"
import ListPaymentsRedux from "./components/ListPaymentsRedux"

import Home from "./components/Home"
import AddPayments from "./components/AddPayments";

//import Welcome from "./components/Welcome";
import ViewPayments from "./components/ViewPayments";
import About from "./components/About"
function App() {
  return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <a href="/Home" className="navbar-brand">
            <img className={"imgLogo"} src={require('./lg.png')} alt="Home"></img>
          </a>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/Payments"} className="nav-link">
                List Payments
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/add"} className="nav-link">
                Add Payment
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/ListPaymentsRedux"} className="nav-link">
                List Payment Redux
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/About"} className="nav-link">
                About
              </Link>
            </li>
          </div>
        </nav>
        <div className="container mt-3">
          <Switch>
            <Route exact path={["/", "/Home"]} component={Home} />
            <Route exact path={["/", "/Payments"]} component={ListPayments} />
            <Route exact path={["/", "/About"]} component={About} />
            <Route exact path={["/", "/ListPaymentsRedux"]} component={ListPaymentsRedux} />
            <Route exact path="/add" component={AddPayments} />
            <Route path="/Payments/:id" component={ViewPayments} />
          </Switch>
        </div>
      </div>
  );
}

export default App;
