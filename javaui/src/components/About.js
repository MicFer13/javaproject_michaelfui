import React, {Component} from 'react';
//var fetch = require('node-fetch');
import "bootstrap/dist/css/bootstrap.min.css";
import './About.css'
export default class About extends Component{
    constructor(props) {
        super(props);
        this.state = {name: "About",  apistatus:"Unknown"}
    }
    render()
    {
        return (<div><h1>Welcome to the {this.state.name} page</h1>
                <h3>Status is {this.state.apistatus}</h3>
        <p1>This is a payment example page </p1>

            <ul className="list-unstyled">
                <li className="media">
                    <img className="mr3" src={require("../CEO.jpg")} alt="Generic placeholder image"/>
                        <div className="media-body">
                            <h5 className="mt-0 mb-1">CEO</h5>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.
                            Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc
                            ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                        </div>
                </li>
                <li className="media my-4">
                    <img className="mr3" src={require("../cto.png")} alt="Generic placeholder image"/>
                        <div className="media-body">
                            <h5 className="mt-0 mb-1">CTO</h5>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.
                            Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc
                            ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                        </div>
                </li>
                <li className="media">
                    <img className="mr3" src={require("../cfo.png")}alt="Generic placeholder image"/>
                        <div className="media-body">
                            <h5 className="mt-0 mb-1">CFO</h5>
                            Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin.
                            Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc
                            ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                        </div>
                </li>
            </ul>

        </div>

        )
    }
    async componentDidMount() {

        let status;
        try {
            let url = 'http://localhost:8080/api/status'
            let response = await fetch(url);
            status = await response.text()
            console.log('The status is: ' + status)
        }
        catch(e)
        {
            console.log(e.toString())
        }
        this.setState({apistatus: status})
    }
}