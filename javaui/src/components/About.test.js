
import React from 'react';
import { render } from '@testing-library/react';
import About from './About';

jest.mock('./About');

test('renders', () => {
    const { container } = render(<About/>);
    expect(container.textContent)
        .toContain('CEO');
});