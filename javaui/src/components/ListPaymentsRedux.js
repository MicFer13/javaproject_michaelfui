import React, {Component} from 'react';
import paymentList from "../redux/actions/PaymentListActions";
import { connect } from 'react-redux';
import PaymentRestService from "../services/PaymentRestService";
//import { bindActionCreators } from 'redux';

class  ListPaymentsRedux extends  Component {
    constructor(props) {
        super(props)
        this.props.dispatch(paymentList())
        console.log(this.props)

    }


    render() {
        const {  payments,pending, error} = this.props;
        return (
            <div className="col-md-6">
                <h4>Payment List Redux</h4>

                <ul className="list-group">
                    {payments &&
                    payments.map((payment, index) => (
                        <li
                            className={
                                "list-group-item " +
                                (index === 0 ? "active" : "")
                            }
                            // onClick={() => this.setActiveTutorial(tutorial, index)}
                            key={index}
                        >
                            {payment.id }   {payment.custId }    {payment.type }  {payment.amount }
                        </li>
                    ))}
                </ul>
            </div>
        )
    }


}

const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};

const mapStateToProps = state => ({
    error:state.error,
    payments: state.payments,
    pending: state.pending
})


export default connect(mapStateToProps,mapDispatchToProps)(ListPaymentsRedux);


//export default ListEmployees;