import React, {Component} from 'react';
import PaymentService from "../services/PaymentRestService";
import {Redirect} from 'react-router-dom';
export default class extends Component {

    constructor(props) {
        super(props);
        this.onChangeid = this.onChangeid.bind(this);
        this.onChangepaymentDate = this.onChangepaymentDate.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangecustId= this.onChangecustId.bind(this);
        this.savePayment = this.savePayment.bind(this);



        this.state = {

            id: 0,
            paymentDate: "",
            type: "",
            custId:0,
            amount:0.0,
            submitted: false
        };
    }


    onChangeid(e) {
        this.setState({
            id: e.target.value
        });
    }

    onChangepaymentDate(e) {
        this.setState({
            paymentDate: e.target.value
        });
    }


    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }

    onChangecustId(e) {
        this.setState({
            custId: e.target.value
        });
    }

    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    savePayment() {
        var data = {
            id: this.state.id,
            paymentDate: this.state.paymentDate,
            type: this.state.type,
            amount: this.state.amount,
            custId: this.state.custId       }

        PaymentService.create(data)
            .then(response => {
                this.setState({
                    // id: response.data.id,
                    //  paymentDate: response.data.paymentDate,
                    //  type: response.data.type,
                    //  amount: response.data.amount,
                    //  custId: response.data.custId,
                    submitted: true

                });

            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }



    render() {
        if(this.state.submitted){
            return <Redirect to ="home" />
        }
        return (<form>
            <h1>Add Payments</h1>
            <div className="form-group">
                <label htmlFor="exid">id</label>
                <input type="number" className="form-control" id="exid" aria-describedby="idHelp"
                       placeholder="Enter name" value={this.state.id} onChange={this.onChangeid}/>
                <small id="idHelp" className="form-text text-muted">We'll never share your details with anyo
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="expaymentDate">paymentDate</label>
                <input type="date" className="form-control" id="expaymentDate" aria-describedby="paymentDateHelp"
                       placeholder="Enter name" value={this.state.paymentDate} onChange={this.onChangepaymentDate}/>
                <small id="paymentDateHelp" className="form-text text-muted">We'll never share your details with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleInputType1">Type</label>
                <input type="text" className="form-control" id="exampleInputType1" aria-describedby="TypeHelp"
                       placeholder="Enter Type" value={this.state.type} onChange={this.onChangeType}/>
                <small id="TypeHelp" className="form-text text-muted">We'll never share your Type with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="examplecustId">custId</label>
                <input type="number" className="form-control" id="examplecustId" aria-describedby="TypeHelp"
                       placeholder="Enter custId" value={this.state.custId} onChange={this.onChangecustId}/>
                <small id="TypecustId" className="form-text text-muted">We'll never share your custIdwith anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleAmount">Amount</label>
                <input type="number" className="form-control" id="exampleAmount" aria-describedby="Typeid"
                       placeholder="Enter Amount" value={this.state.amount} onChange={this.onChangeAmount}/>
            </div>
            <div className="form-group">

                <input type="button" className="form-control" value={"save"} onClick={this.savePayment}/>
            </div>

        </form>)
    }
}

