import React, {Component} from 'react';
//var fetch = require('node-fetch');
import "bootstrap/dist/css/bootstrap.min.css";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";

export default class Home extends Component{
    constructor(props) {
        super(props);
        this.state = {name: "Home"}
    }
    render()
    {
        return (<div><h1>Welcome to the Home page</h1>
                <h3></h3>
                <p1>This is a payment example page </p1>
                <body>
                <p className="comPro">We are uncovering better ways of developing software by doing it and helping others do it.  </p>
                <CardGroup>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 1</Card.Title>
                            <Card.Text>
                                Our highest priority is to satisfy the customer
                                through early and continuous delivery
                                of valuable software.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 2</Card.Title>
                            <Card.Text>
                                Welcome changing requirements, even late in
                                development. Agile processes harness change for
                                the customer's competitive advantage.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 3</Card.Title>
                            <Card.Text>
                                Deliver working software frequently, from a
                                couple of weeks to a couple of months, with a
                                preference to the shorter timescale.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </CardGroup>
                <CardGroup>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 4</Card.Title>
                            <Card.Text>
                                Business people and developers must work
                                together daily throughout the project.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 5</Card.Title>
                            <Card.Text>
                                Build projects around motivated individuals.
                                Give them the environment and support they need,
                                and trust them to get the job done.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 6</Card.Title>
                            <Card.Text>
                                The most efficient and effective method of
                                conveying information to and within a development
                                team is face-to-face conversation.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </CardGroup>
                <CardGroup>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 7</Card.Title>
                            <Card.Text>
                                Working software is the primary measure of progress.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 8</Card.Title>
                            <Card.Text>
                                Agile processes promote sustainable development.
                                The sponsors, developers, and users should be able
                                to maintain a constant pace indefinitely.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 9</Card.Title>
                            <Card.Text>
                                Continuous attention to technical excellence
                                and good design enhances agility.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </CardGroup>
                <CardGroup>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 10</Card.Title>
                            <Card.Text>
                                Simplicity--the art of maximizing the amount
                                of work not done--is essential.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 11</Card.Title>
                            <Card.Text>
                                The best architectures, requirements, and designs
                                emerge from self-organizing teams.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    <Card border="success" style={{ width: '18rem' }}>
                        <Card.Header>News Item (todo)</Card.Header>
                        <Card.Body>
                            <Card.Title>Principle 12</Card.Title>
                            <Card.Text>
                                At regular intervals, the team reflects on how
                                to become more effective, then tunes and adjusts
                                its behavior accordingly.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </CardGroup>
                <br />
                </body>
        </div>
        )
    }
}