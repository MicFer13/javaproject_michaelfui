import React, {Component} from 'react';
import PaymentService from "../services/PaymentRestService";

export default class  ListPayments extends  Component {
    constructor(props) {
        super(props);
        this.state = {
            payments: [],
            currentIndex: -1}
    }
    retrievePayments() {
        PaymentService.getAll()
            .then(response => {
                this.setState({
                    payments: response.data
                });
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }
    componentDidMount() {
        this.retrievePayments();
    }

    render() {
        const {  payments,currentIndex} = this.state;
        return  (
            <div className="col-md-6">
                <h4>Payments List</h4>

                <ul className="list-group">
                    <table class="sortable">

                            <tr>
                                <th>Payment ID</th>
                                <th>Payment Type</th>
                                <th>Amount paid</th>
                                <th>Cust No</th>
                            </tr>

                    {payments &&
                    payments.map((payment, index) => (
                        // <tr
                        //     className={
                        //         "list-group-item " +
                        //         (index === currentIndex ? "active" : "")
                        //     }
                        //     // onClick={() => this.setActiveTutorial(tutorial, index)}
                        //     key={index}
                        // >
                        <tr>
                            <td>{payment.id}</td> <td>{payment.type}</td>
                            <td>${payment.amount}</td> <td>{payment.custId}</td>
                        </tr>


                    ))}
                    </table>
                </ul>
            </div>
        )
    }

}
//export default ListPayments;